import security from '/cypress/support/selectors/security';
import sidebar from '/cypress/support/selectors/sidebar';
		let ip = "111.111.111.11";

///<reference types="cypress" />
		it ("AnyMoney Stage SCI Security Settings", () => {
						cy.auth(Cypress.env('login'), Cypress.env('password'))
						cy.title().should('eq', 'Мерчанты | any.money').wait(1000)
						cy.get(sidebar.Settings).click()
						cy.get(security.Page).click()
						cy.title().should('eq', 'Настройки мерчанта | any.money').wait(1000)
						cy.get(security.ShowSCIKey).click()
						cy.get(security.ModalConfirmButton).click()
						cy.get(security.ModalTitleText).should('contain', 'Предупреждение')
						cy.get(security.ModalConfirmButton).click()
						cy.get(security.ModalTitleText).should('contain', 'Секретный ключ')
						cy.get(security.ModalCloseButton).click()

						cy.get(security.ShowAPIKey).click()
						cy.get(security.ModalConfirmButton).click()
						cy.get(security.ModalTitleText).should('contain', 'Предупреждение')
						cy.get(security.ModalConfirmButton).click()
						cy.get(security.ModalTitleText).should('contain', 'Секретный ключ')
						cy.get(security.ModalCloseButton).click()

						cy.get(security.IPList).should('contain', ip)
						cy.get(security.AddIPInput).type(ip);
						cy.get(security.AddIPButton).click();
						cy.get(security.IPList).should('contain', ip)
						cy.get(security.ModalTitleText).should('contain', 'Ошибка')
						cy.get(security.ModalConfirmButton).click()
						cy.get(security.DeleteIP).click()
						cy.get(security.ModalDescriptionText).should('contain', 'Вы действительно хотите удалить '+ip+' из списка разрешенных адресов?')
						cy.get(security.ModalConfirmButton).click()
						cy.get(security.AddIPInput).type(ip);
						cy.get(security.AddIPButton).click();
						cy.get(security.IPList).should('contain', ip)
						cy.get(sidebar.Merchant).click()
						cy.get(sidebar.Logout).click()
						cy.url().should('contain', '/ru')
			})
