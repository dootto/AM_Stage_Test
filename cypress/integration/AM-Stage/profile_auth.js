import profile from '/cypress/support/selectors/profile_auth';
import sidebar from '/cypress/support/selectors/sidebar';
import l from '/cypress/support/selectors/login';

			let PWD = 'auto-test_Password234';
			let NotificationErrorMsg = 'Превышен лимит попыток подтверждения действия. Повторите попытку через час.';

///<reference types="cypress" />
		it ("AnyMoney Stage Profile Auth", () => {
						cy.auth(Cypress.env('second_login'), Cypress.env('password'))
						cy.title().should('eq', 'Мерчанты | any.money').wait(1000)
						cy.get(sidebar.Profile).click().wait(1000)
						cy.title().should('eq', 'Профиль | any.money').wait(1000)
						cy.get(profile.Auth2FASelect).should('contain', 'Не выбрано')
						cy.get(profile.Auth2FASelect).click()
						cy.get(profile.Auth2FAEmail).click()
						cy.get(profile.Auth2FASwitcherAuth).should('have.class', 'ivu-switch-checked')
						cy.get(profile.Auth2FASwitcherSettings).should('have.class', 'ivu-switch-checked')
						cy.get(profile.Auth2FASwitcherActions).should('have.class', 'ivu-switch-checked')
						cy.get(profile.Auth2FASwitcherAuth).click().should('not.have.class', 'ivu-switch-checked')
						cy.get(profile.Auth2FASwitcherSettings).click().should('not.have.class', 'ivu-switch-checked')
						cy.get(profile.Auth2FASwitcherActions).click().should('not.have.class', 'ivu-switch-checked')
						cy.get(profile.Auth2FASwitcherActions).click().should('have.class', 'ivu-switch-checked')
						cy.get(profile.Auth2FASelect).click()
						cy.get(profile.Auth2FADefault).click()
						cy.get(profile.Auth2FASwitcherAuth).should('have.class', 'ivu-switch-disabled')
						cy.get(profile.Auth2FASwitcherSettings).should('have.class', 'ivu-switch-disabled')
						cy.get(profile.Auth2FASwitcherActions).should('have.class', 'ivu-switch-disabled')
						cy.get(sidebar.Merchant).click()
						cy.get(profile.ModalConfirmButton).click()
						
						cy.get(sidebar.Profile).click()
						cy.get(profile.PWDChange).click()
						cy.get(profile.PWDChangeConfirmNewPWD).type(PWD)
						cy.get(profile.PWDChangeNewPWD).type(PWD)
						cy.get(profile.ModalConfirmButton).click()
						cy.get(sidebar.Merchant).click()
						cy.get(sidebar.Logout).click()
						cy.visit('/ru/authorization')
						cy.get(l.inputLogin)
							.type(Cypress.env('second_login'));
						cy.get(l.inputPassword)
							.type(PWD);
						cy.get(l.buttonLogin).click()
						cy.title().should('eq', 'Мерчанты | any.money').wait(1000)
						cy.get(sidebar.Profile).click()
						cy.title().should('eq', 'Профиль | any.money').wait(1000)
						cy.get(profile.PWDChange).click()
						cy.get(profile.PWDChangeConfirmNewPWD).type(Cypress.env('password'))
						cy.get(profile.PWDChangeNewPWD).type(Cypress.env('password'))
						cy.get(profile.ModalConfirmButton).click()
						cy.get(sidebar.Merchant).click()
						cy.get(sidebar.Logout).click()

						cy.auth(Cypress.env('second_login'), Cypress.env('password'))
						cy.title().should('eq', 'Мерчанты | any.money')
						cy.get(sidebar.Logout).click()
						
})