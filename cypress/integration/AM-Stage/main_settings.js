import sidebar from '/cypress/support/selectors/sidebar';
import ms from '/cypress/support/selectors/main_settings';

				var randomMerchantName = Math.random().toString(36).substr(2, 10)
				var randomMerchantUrl = Math.random().toString(36).substr(2, 5)
				var randomMerchantEmail = Math.random().toString(36).substr(2, 10)
				var randomMerchantCallback = Math.random().toString(36).substr(2, 20)


///<reference types="cypress" />
			it ("AnyMoney Stage Main Settings", () => {
						cy.auth(Cypress.env('login'), Cypress.env('password'))
						cy.title().should('eq', 'Мерчанты | any.money')
						cy.wait(1000)
						cy.get(sidebar.Settings).click()
						cy.url().should('contain', '/settings/main')
						cy.get(ms.MerchantNamePlaceholder).clear()
						cy.get(ms.MerchantName).type(randomMerchantName)
						cy.get(ms.MerchantUrl).clear()
						cy.get(ms.MerchantUrl).type(`https://`+randomMerchantUrl+`.com`)
						cy.get(ms.MerchantEmail).first().clear()
						cy.get(ms.MerchantEmail).first().type(randomMerchantEmail+`@gmail.com`)
						cy.get(ms.MerchantCallback).first().clear()
						cy.get(ms.MerchantCallback).type(`https://`+randomMerchantCallback+`.com`)
						cy.get(ms.ButtonSumbit).click()
						cy.get(ms.NotificationMSG).contains('Сохранено')
						cy.get(sidebar.Merchant).click()
						cy.get(ms.MerchantNameTitle).contains(randomMerchantName)
						cy.get(sidebar.Logout).click()
						cy.url().should('contain', '/ru')
		})