import l from '/cypress/support/selectors/login';
Cypress.Commands.add("auth", (login, password) => {
		cy.visit('/ru/authorization')
		cy.get(l.inputLogin)
			.type(login);
		cy.get(l.inputPassword)
			.type(password);
		cy.get(l.buttonLogin).click()
})
