 	export default {
	Humburger: ".a-icon-button.side-menu--hamburger",
  Merchant: "[at-m-side-menu-item='merchant']",
  Payments: "[at-m-side-menu-item='menu-link-cabinet-lid-payments']",
  Exchanges: "[at-m-side-menu-item='menu-link-cabinet-lid-transactions']",
  Payouts: "[at-m-side-menu-item='menu-link-cabinet-lid-orders-out']",
  History: "[at-m-side-menu-item='menu-link-cabinet-lid-history']",
  Settings: "[at-m-side-menu-item='cabinet-lid-settings-main']",
  Support: "[at-m-side-menu-item='support-link']",
  Profile: "[at-m-side-menu-item='user-profile']",
  Logout: "[at-m-side-menu-item='logout']"}