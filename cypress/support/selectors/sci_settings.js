export default {
	Page: "[at-a-tab-element='cabinet-lid-settings-sci']",
  ThemeAuto: ":nth-child(1) > .a-switch",
  ThemeDay: ":nth-child(2) > .a-switch",
  ThemeNight: ":nth-child(3) > .a-switch",
  Language: "[class='multiselect__tags']",
  LanguageEN: "[at-multi-select='en']",
  LanguageRU: "[at-multi-select='ru']",
  LanguageUK: "[at-multi-select='uk']",
  ButtonSubmit: "[at-a-button='save-settings']",
  PopUp: "[class='base-modal-body']",
  PopUpConfirm: "[at-a-button='modal-confirm-button']"
}