export default {
	Create: ".a-button.button--small.button--primary.button--full-width.button--icon",
	CreatePayway: "[at-m-select-payway='create-order-select']",
	CreatePaywayAnycash: "[at-multi-select='anycash']",
	CreateAnyCashCode: "[at-a-input-text='create-payment-code']",
	CreateSumbit: "[at-a-button='create-payment-submit']",
	DrowerClose: "a.ivu-drawer-close",
	DrowerCloseConfirm: "[at-a-button='modal-confirm-button']"
}

