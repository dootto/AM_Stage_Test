export default {
	Auth2FASelect: "[at-a-select='two-factor-authentication']",
	Auth2FAEmail: "[at-multi-select='email']",
	Auth2FADefault: "[at-multi-select='default']",
	Auth2FASwitcherAuth: "[at-a-switch='security-auth2-login']",
	Auth2FASwitcherSettings: "[at-a-switch='security-auth2-settings']",
	Auth2FASwitcherActions: "[at-a-switch='security-auth2-actions']",
	Auth2FASumbitButton: "[at-a-button='save-auth2-button']",
	AuthModalTitleText: ".base-modal-title__text",
	AuthModalCanseledButton: "[at-a-button='modal-cancel-button']",
	PWDChange: "[at-a-button='change-password-modal']",
	PWDChangeNewPWD: "[at-a-input-text='password']",
	PWDChangeConfirmNewPWD: "[at-a-input-text='confirm-password']",
	ModalConfirmButton: "[at-a-button='modal-confirm-button']",
	NotificationClose: ".ivu-icon.ivu-icon-ios-close",
	NotificationMsg: ".notification__message",
	LK: "a-link.link.header__link.header__link--login.link--bold"
}

