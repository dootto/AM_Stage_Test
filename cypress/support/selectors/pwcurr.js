export default {
	CurrInPage: "[at-a-tab-element='cabinet-lid-settings-pwcurrency-in']",
	CurrOutPage: "[at-a-tab-element='cabinet-lid-settings-pwcurrency-out']",

	CurrInAnyCash: "[at-a-switch='pwcurrency-status-checkbox-anycash']",
	CurrOutAnyCash: "[at-a-switch='pwcurrency-status-checkbox-anycash']"
}



