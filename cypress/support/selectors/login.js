export default {
	inputLogin: "[at-a-tooltip-wrapper='login']",
  inputPassword: "[at-a-tooltip-wrapper='password']",
	buttonLogin: "[at-a-button='login-btn']"
}