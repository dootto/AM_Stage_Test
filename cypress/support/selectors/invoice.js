export default {
	Page: "[at-a-tab-element='cabinet-lid-payments-invoices']",
  Create: ".a-button.payments-invoices__create-btn.button--small.button--primary.button--full-width.button--icon",
  CreateAmount: "[at-a-input-number='invoice-create-in-amount']",
  CreateEmail: "[at-a-tooltip-wrapper='invoice-create-email']",
	CreateDescription: "[at-a-tooltip-wrapper='invoice-create-note']",
  CreateSubmit: "[at-a-button='create-invoice-submit']",
  CreateConfirm: ".a-button.invoice-create-confirm__submit-button.button--large.button--primary",
  CopyPayLink: ".o-invoice-details.o-invoice-details-drawer",
  DrowerClose: "a.ivu-drawer-close",

	CheckAmount: "[at-a-info-item='invoice-create-confirm-in-amount']",
	CheckEmail: "[at-a-info-item='invoice-create-confirm-email']",
	CheckDescription: "[at-a-info-item='invoice-create-confirm-note']"
}