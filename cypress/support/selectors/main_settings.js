export default {
	MerchantNamePlaceholder: "[placeholder='Название мерчанта']",
  MerchantName: "[at-a-tooltip-wrapper='merchant-name']",
  ButtonSumbit: "[type='submit']",
  NotificationMSG: "[class='notification__message']",
	MerchantNameTitle: ".merchants-header__title-text",
	MerchantUrl: "[at-a-input-text='merchant-main-url']",
	MerchantEmail: "[at-a-tooltip-wrapper='merchant-email']",
	MerchantCallback: "[at-a-input-text='merchant-callback-url']"
}