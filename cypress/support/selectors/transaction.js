export default {
	Page: "[at-a-tab-element='cabinet-lid-settings-transaction']",
	TransDelaySelect: "[at-a-select='select-transaction-delay']",
	TransDelaySubmitButton: "[at-a-button='submit-transaction-delay']",
	Delay0Sec: "[at-multi-select='0']",
	Delay20Sec: "[at-multi-select='20000']",
	Delay60Sec: "[at-multi-select='60000']",
	Delay3Min: "[at-multi-select='180000']",
	Delay5Min: "[at-multi-select='300000']",
	Delay10Min: "[at-multi-select='600000']",
	DelaySwitch: "at-a-switch='switch-transaction-delay']",
	DelayAmount: "[at-a-input-number='amount-transaction-delay']",
	DelayAmountInput: ".input-number__input",
	DelayNotification: "[at-a-notification='']",
	DelayChose: ".multiselect__single"
}



