export default {
	Page: "[at-a-tab-element='cabinet-lid-settings-security']",
	ShowSCIKey: "[at-a-button='show-merchant-private-key-sci']",
	ShowAPIKey: "[at-a-button='show-merchant-private-key-api']",
	AddIPInput: "[at-a-input-text='merchant-ip-address']",
	AddIPButton: "[at-a-button='merchant-add-ip-address']",
	ModalCloseButton: "[at-a-button='modal-cancel-button']",
	ModalConfirmButton: "[at-a-button='modal-confirm-button']",
	ModalTitleText: ".base-modal-title__text",
	ModalDescriptionText: ".base-modal-description",
	IPList: ".settings__ip-list",
	DeleteIP: ".a-icon-button.tag-button__icon"
}